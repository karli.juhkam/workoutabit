export let visitors = [
    {
        name: 'Kristi Ilves',
        email: 'kristi@gmail.com',
        training_type: 'Jõusaal',
        visit_amount: 10,
        valid_until: '06.09.2021',
        last_visited: '10.08.2021',
        document_number: 'AE1111222',
        active_package: '10-kordne',
        max_visit: 10
    },
    {
        name: 'Kaire Hunt',
        email: 'kaire@hot.ee',
        training_type: 'Jõusaal',
        visit_amount: 0,
        valid_until: '20.09.2021',
        last_visited: '-',
        document_number: 'AE1111232',
        active_package: '1-kordne',
        max_visit: 1
    },
    {
        name: 'Mari Karu',
        email: 'mari@mail.ee',
        training_type: 'Jõusaal',
        visit_amount: 14,
        valid_until: '31.09.2021',
        last_visited: '20.08.2021',
        document_number: 'AE1111262',
        active_package: '30-päeva',
        max_visit: 999
    }
];